package com.example.ahmadqosam.footballmatchschedule.activity

import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.Espresso.pressBack
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.swipeLeft
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.example.ahmadqosam.footballmatchschedule.R
import com.example.ahmadqosam.footballmatchschedule.R.id.viewpager_main
import com.example.ahmadqosam.footballmatchschedule.adapter.Ids.match_container
import com.example.ahmadqosam.footballmatchschedule.view.MatchListUI.Ids.list_match
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest{
    @Rule
    @JvmField var activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testRecyclerViewBehaviour() {
        onView(allOf(withId(list_match), isDisplayed()))
        Thread.sleep(1500)
        onView(allOf(withId(list_match), isDisplayed())).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(14))
        onView(allOf(withId(list_match), isDisplayed())).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(14, click()))
    }

    @Test
    fun testAppBehaviour() {
        val viewPager = onView(withId(viewpager_main))
            .check(matches(isDisplayed()))

        val firstItemOfLastMatch = onView(
            allOf(
                childAtPosition(
                    childAtPosition(
                        withClassName(Matchers.`is`("org.jetbrains.anko._RelativeLayout")),
                        0
                    ),
                    0
                ),
                isDisplayed()
            )
        )

        Thread.sleep(1500)
        firstItemOfLastMatch.perform(click())

        val favoriteButton = onView(
            allOf(
                withId(R.id.add_to_favorite), withContentDescription("favorites"),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.action_bar),
                        1
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        Thread.sleep(1500)
        favoriteButton.perform(click())
        onView(withText(R.string.favorite_success))
            .check(matches(isDisplayed()))

        pressBack()

        viewPager.perform(swipeLeft())
        viewPager.perform(swipeLeft())

        val firstItemOfFavoriteMatch = onView(
            allOf(
                childAtPosition(
                    childAtPosition(
                        withClassName(Matchers.`is`("org.jetbrains.anko._RelativeLayout")),
                        0
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        Thread.sleep(1500)
        firstItemOfFavoriteMatch.perform(click())

        Thread.sleep(1500)
        favoriteButton.perform(click())
        onView(withText(R.string.unfavorite_success))
            .check(ViewAssertions.matches(isDisplayed()))
    }

    private fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}