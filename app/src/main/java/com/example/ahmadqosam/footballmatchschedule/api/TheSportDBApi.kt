package com.example.ahmadqosam.footballmatchschedule.api

import android.net.Uri
import com.example.ahmadqosam.footballmatchschedule.BuildConfig

object TheSportDBApi {

    fun getLastMatches(/*id: String?*/): String {
        return "${BuildConfig.BASE_URL}api/v1/json/${BuildConfig.TSDB_API_KEY}/eventspastleague.php?id=4328"
    }

    fun getNextMatches(/*id: String?*/): String {
        return "${BuildConfig.BASE_URL}api/v1/json/${BuildConfig.TSDB_API_KEY}/eventsnextleague.php?id=4328"
    }

    fun getMatchData(matchId: String?): String {
        return "${BuildConfig.BASE_URL}api/v1/json/${BuildConfig.TSDB_API_KEY}/lookupevent.php?id=${matchId}"
    }

    fun getTeam(teamId: String?): String {
        return "${BuildConfig.BASE_URL}api/v1/json/${BuildConfig.TSDB_API_KEY}/lookupteam.php?id=${teamId}"
    }

}