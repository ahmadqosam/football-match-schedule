package com.example.ahmadqosam.footballmatchschedule.presenter

import com.example.ahmadqosam.footballmatchschedule.api.ApiRepository
import com.example.ahmadqosam.footballmatchschedule.api.TheSportDBApi
import com.example.ahmadqosam.footballmatchschedule.model.MatchResponse
import com.example.ahmadqosam.footballmatchschedule.model.TeamResponse
import com.example.ahmadqosam.footballmatchschedule.utils.CoroutineContextProvider
import com.example.ahmadqosam.footballmatchschedule.view.MainView
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainPresenter(private val view: MainView,
                    private val apiRepository: ApiRepository,
                    private val gson: Gson,
                    private val context: CoroutineContextProvider = CoroutineContextProvider()
) {

    fun getLastMatchList() {
        view.showLoading()
        GlobalScope.launch(context.main){
            val data = gson.fromJson(apiRepository
                .doRequest(TheSportDBApi.getLastMatches()).await(),
                MatchResponse::class.java)

            view.showMatchData(data.events)
            view.hideLoading()
        }
    }

    fun getNextMatchList() {
        view.showLoading()
        GlobalScope.launch(context.main){
            val data = gson.fromJson(apiRepository
                .doRequest(TheSportDBApi.getNextMatches()).await(),
                MatchResponse::class.java)

            view.showMatchData(data.events)
            view.hideLoading()
        }
    }

    fun getMatchData(eventId: String){
        view.showLoading()
        GlobalScope.launch(context.main){
            val data = gson.fromJson(apiRepository
                .doRequest(TheSportDBApi.getMatchData(eventId)).await(),
                MatchResponse::class.java)

            view.showMatchData(data.events)
            view.hideLoading()
        }
    }


    fun getHomeTeamData(teamId: String){
        view.showLoading()
        GlobalScope.launch(context.main){
            val data = gson.fromJson(apiRepository
                .doRequest(TheSportDBApi.getTeam(teamId)).await(),
                TeamResponse::class.java)

            view.showHomeTeamData(data.teams)
            view.hideLoading()
        }
    }

    fun getAwayTeamData(teamId: String){
        view.showLoading()
        GlobalScope.launch(context.main){
            val data = gson.fromJson(apiRepository
                .doRequest(TheSportDBApi.getTeam(teamId)).await(),
                TeamResponse::class.java)

            view.showAwayTeamData(data.teams)
            view.hideLoading()
        }
    }
}