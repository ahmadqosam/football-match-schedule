package com.example.ahmadqosam.footballmatchschedule.utils

import android.view.View

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun isNotEmptyOrNull(value: String?): Boolean{
    return (value != "null" && value != "" && value != null)
}