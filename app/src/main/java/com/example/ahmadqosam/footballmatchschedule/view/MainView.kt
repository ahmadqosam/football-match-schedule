package com.example.ahmadqosam.footballmatchschedule.view

import com.example.ahmadqosam.footballmatchschedule.model.Match
import com.example.ahmadqosam.footballmatchschedule.model.Team

interface MainView {
    fun showLoading()
    fun hideLoading()
    fun showMatchData(data: List<Match>)
    fun showHomeTeamData(data: List<Team>)
    fun showAwayTeamData(data: List<Team>)

}