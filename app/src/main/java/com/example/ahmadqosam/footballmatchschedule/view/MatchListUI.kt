package com.example.ahmadqosam.footballmatchschedule.view

import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.example.ahmadqosam.footballmatchschedule.R
import com.example.ahmadqosam.footballmatchschedule.adapter.MainAdapter
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.swipeRefreshLayout

class MatchListUI<T>(private val mainAdapter: MainAdapter) : AnkoComponent<T> {

    private lateinit var listTeam: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var swipeRefresh: SwipeRefreshLayout


    override fun createView(ui: AnkoContext<T>) = with(ui) {
        linearLayout {
            lparams (width = matchParent, height = wrapContent)
            orientation = LinearLayout.VERTICAL

            swipeRefresh = swipeRefreshLayout {

                id = Ids.swipe_refresh

                setColorSchemeResources(
                    R.color.colorAccent,
                    android.R.color.holo_green_light,
                    android.R.color.holo_orange_light,
                    android.R.color.holo_red_light)

                relativeLayout{
                    lparams (width = matchParent, height = wrapContent)
                    gravity = Gravity.CENTER

                    listTeam = recyclerView {
                        id = Ids.list_match
                        lparams (width = matchParent, height = wrapContent)
                        layoutManager = LinearLayoutManager(context)
                        adapter = mainAdapter
                    }

                    progressBar = progressBar {
                        id = Ids.progress_bar
                    }.lparams{
                        centerHorizontally()
                    }
                }
            }
        }
    }

    object Ids {
        const val progress_bar = 1
        const val swipe_refresh = 2
        const val list_match = 99
    }
}