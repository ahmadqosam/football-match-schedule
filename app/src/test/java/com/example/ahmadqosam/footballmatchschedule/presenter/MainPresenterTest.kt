package com.example.ahmadqosam.footballmatchschedule.presenter

import com.example.ahmadqosam.footballmatchschedule.TestContextProvider
import com.example.ahmadqosam.footballmatchschedule.api.ApiRepository
import com.example.ahmadqosam.footballmatchschedule.api.TheSportDBApi
import com.example.ahmadqosam.footballmatchschedule.model.Match
import com.example.ahmadqosam.footballmatchschedule.model.MatchResponse
import com.example.ahmadqosam.footballmatchschedule.model.Team
import com.example.ahmadqosam.footballmatchschedule.model.TeamResponse
import com.example.ahmadqosam.footballmatchschedule.view.MainView
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Test
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class MainPresenterTest {

    @Mock
    private
    lateinit var view: MainView

    @Mock
    private
    lateinit var gson: Gson

    @Mock
    private
    lateinit var apiRepository: ApiRepository

    private lateinit var presenter: MainPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = MainPresenter(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun getLastMatchList() {
        val matches: MutableList<Match> = mutableListOf()
        val response = MatchResponse(matches)

        GlobalScope.launch {
            `when`(gson.fromJson(apiRepository
                .doRequest(TheSportDBApi.getLastMatches()).await(),
                MatchResponse::class.java
            )).thenReturn(response)

            presenter.getLastMatchList()

            Mockito.verify(view).showLoading()
            Mockito.verify(view).showMatchData(matches)
            Mockito.verify(view).hideLoading()
        }
    }

    @Test
    fun getNextMatchList() {
        val matches: MutableList<Match> = mutableListOf()
        val response = MatchResponse(matches)

        GlobalScope.launch {
            `when`(gson.fromJson(apiRepository
                .doRequest(TheSportDBApi.getNextMatches()).await(),
                MatchResponse::class.java
            )).thenReturn(response)

            presenter.getNextMatchList()

            Mockito.verify(view).showLoading()
            Mockito.verify(view).showMatchData(matches)
            Mockito.verify(view).hideLoading()
        }
    }

    @Test
    fun getMatchData() {
        val matches: MutableList<Match> = mutableListOf()
        val response = MatchResponse(matches)
        val id = "1234"

        GlobalScope.launch {
            `when`(gson.fromJson(apiRepository
                .doRequest(TheSportDBApi.getMatchData(id)).await(),
                MatchResponse::class.java
            )).thenReturn(response)

            presenter.getMatchData(id)

            Mockito.verify(view).showLoading()
            Mockito.verify(view).showMatchData(matches)
            Mockito.verify(view).hideLoading()
        }
    }

    @Test
    fun getHomeTeamData() {
        val teams: MutableList<Team> = mutableListOf()
        val response = TeamResponse(teams)
        val id = "1234"

        GlobalScope.launch {
            `when`(gson.fromJson(apiRepository
                .doRequest(TheSportDBApi.getTeam(id)).await(),
                TeamResponse::class.java
            )).thenReturn(response)

            presenter.getHomeTeamData(id)

            Mockito.verify(view).showLoading()
            Mockito.verify(view).showHomeTeamData(teams)
            Mockito.verify(view).hideLoading()
        }
    }

    @Test
    fun getAwayTeamData() {
        val teams: MutableList<Team> = mutableListOf()
        val response = TeamResponse(teams)
        val id = "1234"

        GlobalScope.launch {
            `when`(gson.fromJson(apiRepository
                .doRequest(TheSportDBApi.getTeam(id)).await(),
                TeamResponse::class.java
            )).thenReturn(response)

            presenter.getAwayTeamData(id)

            Mockito.verify(view).showLoading()
            Mockito.verify(view).showAwayTeamData(teams)
            Mockito.verify(view).hideLoading()
        }
    }
}