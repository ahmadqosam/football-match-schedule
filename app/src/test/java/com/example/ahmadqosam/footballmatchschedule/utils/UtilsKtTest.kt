package com.example.ahmadqosam.footballmatchschedule.utils

import org.junit.Test

import org.junit.Assert.*

class UtilsKtTest {

    @Test
    fun testIsNotEmptyOrNull() {
        val arg1 = ""
        assertEquals(false, isNotEmptyOrNull(arg1))
    }
}